<?php

namespace App;
use App\Stock;
use Illuminate\Database\Eloquent\Model;
use DB;
class Product extends Model
{
    public function stock()
    {
        return $this->hasOne(Stock::class);
    }

    public function kategori()
    {
        return $this->hasOne(DB::table('category'), 'id_category','category');
    }
}