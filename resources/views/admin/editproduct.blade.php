@extends('layouts.admin')

@section ('content')

<div class="col-12 col-md-12 col-sm-12 col-lg-10">
    <h5>EDIT PRODUCT</h5>
    <hr>

    <form method="POST" action="{{ route('product.edit',['id'=>$product->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="row ">

            <div class="col-12">
                <label for="name" class="">{{ __('Name') }}</label>
                <div class="form-group">
                    <div>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $product->name}}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="col-12">
                <label for="price" class="">{{ __('Price') }}</label>
                <div class="form-group">
                    <div>
                        <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') ?? $product->price  }}" required autocomplete="price" autofocus>
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="col-12">
                <label for="type" class="">{{ __('Type') }}</label>
                <div class="form-group">
                    <div>
                        <select name="type" id="addproducttype" class="form-control">
                            <option selected="true" value="" disabled hidden>{{ old('type') ?? \DB::table('type')->where('id_type', $product->type)->first()->type??''  }}</option>
                             @foreach($type as $p)            
                            <option value="{{$p->id_type}}">{{$p->type}}</option>        
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <label for="category" class="">{{ __('Category') }}</label>
                <div class="form-group">
                    <div>
                        <select name="category" id="addproductcategory" class="form-control">
                            <option selected="true" value="" disabled hidden>{{ old('category') ?? \DB::table('category')->where('id_category', $product->category)->first()->category??''  }}</option>                            
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <label for="description" class="">{{ __('Description') }}</label>
                <div class="form-group">
                    <div>
                        <textarea name="description" id="addproductdescription" class="form-control"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="image" class="">Product Image</label>
                    <input type="file" class="form-control" id="image" name="image">
                    @error('image')

                    <div style="color:red; font-weight:bold; font-size:0.7rem;">{{ $message }}</div>

                    @enderror
                </div>

                <img style="height:100px;" src="{{ asset('/storage/'.$product->image) }}" alt=""><br><br>
            </div>
            


        </div>
        
        <button type="submit" class="btn btn-success w-100">EDIT PRODUCT</button>

    </form>

</div>

@endsection

@section('script')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>
  $(".types").change(function() {
    var id = $(".types option:selected").val();
    $.ajax({
        url:"{{ route('category.byId') }}",
        method:'POST',
        data:{
           id:id,
       },
       dataType:'json',
       success:function(data)
       {
        $('#addproductcategory').empty();
        $.each(data, function( index, value ) {
            // var html = `<option selected="true" value="`+value.id_category+`" disabled hidden>`+value.category+`</option>`;
          
        $('#addproductcategory').append(`<option selected="true" value="`+value.id_category+`">`+value.category+`</option>`);
      });
    }
});
});
</script>
@endsection