<footer>
        <div class='container-fluid footer'>
            <div class='container p-0 pt-3'>
                <div class='row'>
                    <div class='col-md-4 col-sm-12 pt-3'>
                        <h3>Contact Information</h3>
                        <p>Faqih Dzaki, Heyri Hermawan<br> Puspowarno 50A <br> +6285290503635 <br> bookstore@gmail.com</p>
                    </div>
                    <div class='col-md-4 col-sm-12 pt-3'>
                        <h3>Follow Us On</h3>
                        <ul>
                            <li><a href='https://facebook.com/' target='_blank'>
                                <i class="fa fa-instagram"></i></a></li>
                            <li><a href='https://instagram.com/' target='_blank'>
                                <i class="fa fa-facebook"></i></a>
                            </li>
                        </ul>

                    </div>
                    <div class='col-md-4 col-sm-12 pt-3'>
                        <h3>Newsletter</h3>
                        <p>Sign up for our newsletter.</p>
                        <div class='newsletter-form p-0'>
                            <form action='{{ route('newsletter.add') }}' method='post' id='newsletter-validate-detail'>
                                @csrf
                                <input type='email' name='email' id='newsletter-footer' class=''
                                    placeholder='Enter your email'>
                                <button type='submit' id='signup-newsletter-footer' class='button'>SIGN UP</button>
                            </form>
                        </div>
                    </div>
                    <div class='col-12 divider-footer p-0'>
                    </div>
                    <div class='col-md-6 col-sm-12 copyright'>
                        <p>Designed Magang</p>
                        <p>CANCreative &copy; 2021</p>
                    </div>
                    <div class='col-md-6 col-sm-12 payment'> <img src="{{ asset('photo/cards.png') }}" alt=''>
                    </div>
                    <div class='col-12 p-0 mt-3'>
                    </div>

                </div>
            </div>
        </div>
    </footer>
